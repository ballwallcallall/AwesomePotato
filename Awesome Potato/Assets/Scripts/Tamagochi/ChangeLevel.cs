﻿using UnityEngine;
using System.Collections;

public class ChangeLevel : MonoBehaviour {
   public  tamagochi_statsHandle stats;

    public void OnChangeLevel()
    {
        //float debuffs = stats.stats.sleepiness + stats.stats.hunger + stats.stats.hygine + stats.stats.happiness;
        //debuffs /= 100;
        //int dist=NabuHook.Instance.GetDistanceWalkedPast24Hours();
        //float debuffs = dist / 10;

        stats.stats.sleepiness -= 20.0f;
        if (stats.stats.sleepiness < 0)
            stats.stats.sleepiness = 5;

        GameObject lt = GameObject.Find("LevelTransition");
        lt.GetComponent<FadeScene>().FadeIn(lt.GetComponent<FadeScene>().fadeInTime);
    }

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
