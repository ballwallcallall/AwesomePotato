﻿using UnityEngine;
using System.Collections;

public class SliderColour : MonoBehaviour {
    public Sprite green;
    public Sprite orange;
    public Sprite red;
    UnityEngine.UI.Slider s;
   public  UnityEngine.UI.Image i;
    // Use this for initialization
	void Start () {
        s = GetComponent<UnityEngine.UI.Slider>();
    }
	
	// Update is called once per frame
	void Update () {
        if(s.value>0.7)
        {
            i.sprite=green;
        }
        else if (s.value > 0.3)
        {
            i.sprite = orange;
        }
        else
        {
            i.sprite = red;
        }
	}
}
