﻿using UnityEngine;
using System.Collections;

public class FortuneWheel : MonoBehaviour {
    public int Next=3;
    public float TimeLeft=4.0f;
    bool slow = false;
    bool stop=false;
    public float timeleft2;
    Animator anim;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!slow && !stop)
        {
            anim.speed = 2.0f;
        }
        else if (slow)
        {
            if (stop)
            {
                anim.speed = 0.0f;
            }
            else
                anim.speed = 1.0f;
        }
        TimeLeft -= Time.deltaTime;
        if (TimeLeft <= 0)
        {
            int t = (Next + 1) % 4+1;//(next+4-2)%4 to get the result of next-2 with it looping
            string a = "s" + t.ToString();
            Debug.Log(a);
            if (stop)
            {
                timeleft2 -= Time.deltaTime;
                if (timeleft2 <= 0)
                {
                    anim.speed = 1.0f;
                    stop = false;
                    slow = false;
                    gameObject.SetActive(false);
                }
            }
            else if (slow)
            {
                a = "s" + (Next+1).ToString();
                if (anim.GetCurrentAnimatorStateInfo(0).IsName(a))
                {
                    anim.speed = 0;
                    stop = true;
                    timeleft2 = 5.0f;
                }
            }
            else if (anim.GetCurrentAnimatorStateInfo(0).IsName(a))
            {
                slow = true;
                anim.speed = 0.5f;
            }
        }
	}
}
