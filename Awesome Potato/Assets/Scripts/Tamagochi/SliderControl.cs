﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SliderControl : MonoBehaviour {
    public Slider hunger;
    public Slider sleep;
    public Slider hygine;
    public Slider happiness;
    tamagochi_statsHandle stat2;
    // Use this for initialization
	void Start () {
        stat2 = GetComponent<tamagochi_statsHandle>();
	}
	
	// Update is called once per frame
	void Update () {
        tamagochiStats stat = stat2.stats;
        hunger.value = stat.hunger * 0.01f;
        sleep.value = stat.sleepiness * 0.01f;
        hygine.value = stat.hygine * 0.01f;
        happiness.value = stat.happiness * 0.01f;
	}
}
