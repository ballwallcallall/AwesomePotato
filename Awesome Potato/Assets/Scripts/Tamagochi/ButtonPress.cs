﻿using UnityEngine;
using System.Collections;

public class ButtonPress : MonoBehaviour {
    public bool pressable = true;
    public UnityEngine.UI.Image darken;
    public tamagochi_statsHandle statHandle;
    public int type;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void OnButton()
    {
        if (pressable)
        {
            if (type == 0)
            {
                pressable = false;
                darken.enabled = true;
                statHandle.feed();
            }
            else if (type == 1)
            {
                statHandle.gsleep();
            }
            else if (type == 2)
            {
                statHandle.Bath();
            }
        }
    }
}
