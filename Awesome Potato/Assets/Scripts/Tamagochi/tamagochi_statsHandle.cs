using UnityEngine;
using System.Collections;



[System.Serializable]
public class tamagochiStats
{
	public float hunger;
	public float timeToPoop;
	public float sleepiness;//0 means really need to sleep
	public float hygine;
	public bool sleeping;

	public float pumped;
	public float happiness;//calcualated based on sleep, hunger and poop
    public int foodType;
    public tamagochiStats()
	{
		hunger = 100.0f;
		timeToPoop = 1000.0f;//handled in seconds
		sleepiness = 100.0f;//start giving negative
		happiness = 100.0f;
		hygine = 100.0f;
        foodType = 0;
		sleeping = false;
	}
}

public class tamagochi_statsHandle : MonoBehaviour {
	public tamagochiStats stats;
	public float RODhunger=0.01f;//persecond
	public float RODhunger2=0.003f;//during inactive
	public float RODSleep=0.0046f;//0-100//6hours
	public float PoopTime;
    public Animator anim;
    public GameObject fortuneWheel;
    public FortuneWheel fScript;
    float timeb4Sleep=0;
    public float timeleft = 0.0f;

	const int hourInSec = 3600;

	// Use this for initialization
	void Start () {
        //anim = GetComponent<Animator>();
		stats = new tamagochiStats ();
        fScript = fortuneWheel.GetComponent<FortuneWheel>();
        LoadStats();
        CalcStats(PlayerPrefs.GetInt("TimeStamp"), stats.sleeping);
		//sync local stats with online stats
		//load stats from local store
	}

	void CalcStats(long LastTime,bool LastSleepCheck)
	{
		System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
		int curTime = (int)((System.DateTime.UtcNow-epochStart).TotalSeconds);
		
		long t = curTime - LastTime;
		Debug.Log (t);
		long timePassed = curTime - LastTime;
		int sleepCycle = hourInSec*6;//6 hours in seconds
		bool currentState = LastSleepCheck;
		int sleepLeft=0;//for calculating the sleep during the first cycle
		while (timePassed>0) {
			if(!currentState)//not sleeping
			{
				//sleepLeft=(int)((stats.sleepiness-50)*0.02f*sleepCycle);//basically calculate how much sleep is left
				sleepLeft=(int)((stats.sleepiness-50)*sleepCycle*0.01);
				if(timePassed>sleepLeft)
				{
					timePassed-=sleepLeft;
					currentState=!currentState;
					stats.sleepiness=50;
				}
				else{
					stats.sleepiness-=RODSleep*timePassed;
					timePassed=0;
					if(stats.sleepiness<=0)
						stats.sleepiness=0;
					if(stats.sleepiness<=50)
						currentState=true;
				}
			}
			else
			{
				if(stats.sleepiness<0)
				{
					if(timePassed<sleepCycle)
					{
						stats.sleepiness*=(sleepCycle-timePassed)/sleepCycle;
					}
					else{//means enough time has passed to reset the thing
						stats.sleepiness=0;
						timePassed-=(long)(sleepCycle*0.5f);
					}
				}
				sleepLeft=(int)((100-stats.sleepiness)*sleepCycle*0.01);//basically calculate how much sleep is left
				if(timePassed>sleepLeft)
				{
					timePassed-=sleepLeft;
					currentState=!currentState;
					stats.sleepiness=100;
				}
				else{
					stats.sleepiness+=RODSleep*timePassed;
					timePassed=0;
					if(stats.sleepiness>100)
						stats.sleepiness=100;
				}
			}
		}
		stats.sleeping = currentState;
        if (currentState)
        {
            gsleep();
        }
        else
            Wake();

        stats.hunger -= t * RODhunger2;
        stats.hygine -= t * RODhunger2;
		if (stats.hunger < 0)
			stats.hunger = 0;
		CalcHappiness ();
	}
	void CalcHappiness()
	{
		stats.happiness = stats.sleepiness + stats.hunger+stats.hygine;
        stats.happiness /= 3;
		stats.happiness=Mathf.Clamp (stats.happiness, 0, 100);
        anim.SetFloat("Happiness", stats.happiness);
	}

	public void SetTimePassed(int nTime)
	{
		System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
		int curTime = (int)((System.DateTime.UtcNow-epochStart).TotalSeconds);
		curTime -= nTime;
		CalcStats (curTime, stats.sleeping);
	}

	public void flipSleep()
	{
		stats.sleeping = !stats.sleeping;
	}

    public void gsleep()
    {
        //Trigger Sleeping event
        if (!anim.GetCurrentAnimatorStateInfo(0).IsTag("NonInterupt"))
        {
            stats.sleeping = true;
            anim.SetBool("Sleeping", true);
            timeb4Sleep = 1.0f;
        }
    }
    public void Wake()
    {
        //Trigger Sleeping event
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("sleeping"))
        {
            if (timeb4Sleep <= 0)
            {
                stats.sleeping = false;
                anim.SetBool("Sleeping", false);
            }
        }
    }

    public void Bath()
    {
        //set anim
        if (!anim.GetCurrentAnimatorStateInfo(0).IsTag("NonInterupt"))
        {
            anim.SetTrigger("Washing");
            stats.hygine = 100;
        }
    }

	public void feed()
	{
        int r=Random.Range(0, 4);
        if (r <= 1)
        {
            stats.hunger += (float)20;
            stats.hunger = Mathf.Clamp(stats.hunger, 0, 100);
        }
        else if (r == 2)
        {
            stats.hunger += (float)30;
            stats.hunger = Mathf.Clamp(stats.hunger, 0, 100);
            stats.foodType = 1;
        }
        else
        {
            stats.hunger += (float)30;
            stats.hunger = Mathf.Clamp(stats.hunger, 0, 100);
            stats.foodType = 2;
        }
        fortuneWheel.SetActive(true);
        fScript.TimeLeft = 5.0f;
        fScript.Next = r;
        anim.SetBool("Eating", true);
        //instatiate spin wheel obj
        timeleft = 20.0f;
	}

	public void accelerate()
	{
		stats.hunger -= RODhunger * Time.deltaTime;
        stats.hygine -= RODhunger * Time.deltaTime;
		if (stats.hunger < 0)
			stats.hunger = 0;
		if (stats.sleeping)
			stats.sleepiness += RODSleep * 2 * Time.deltaTime;
		else
			stats.sleepiness -= RODSleep * Time.deltaTime;

        CalcHappiness();
	}


	// Update is called once per frame
	void Update () {
		accelerate ();
        timeleft -= Time.deltaTime;
		if(stats.sleepiness>=99)
			stats.sleeping=false;
        timeb4Sleep -= Time.deltaTime;
        if (stats.hygine < 30)
        {
            anim.SetBool("Smell", true);
        }
        else
        {
            anim.SetBool("Smell", false);
        }
        if (stats.sleepiness < 30)
        {
            anim.SetBool("Tired", true);
        }
        else
        {
            anim.SetBool("Tired", false);
        }
        foreach (Touch t in Input.touches)
            Wake();
        if (timeleft <= 0)
            anim.SetBool("Eating", false);
        SaveStats();
    }

    void SaveStats()
    {
        System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
		int curTime = (int)((System.DateTime.UtcNow-epochStart).TotalSeconds);
        PlayerPrefs.SetInt("TimeStamp", curTime);
        PlayerPrefs.SetFloat("Hunger", stats.hunger);
        PlayerPrefs.SetFloat("Sleepy", stats.sleepiness);
        PlayerPrefs.SetFloat("Clean", stats.hygine);
        if (stats.sleeping)
        {
            PlayerPrefs.SetInt("sleeping", 1);
        }
        else
        {
            PlayerPrefs.SetInt("sleeping", 0);
        }
        PlayerPrefs.Save();
    }

    void LoadStats()
    {
        stats.hunger=PlayerPrefs.GetFloat("Hunger");
        stats.hygine = PlayerPrefs.GetFloat("Clean");
        stats.sleepiness = PlayerPrefs.GetFloat("Sleepy");
        int i = PlayerPrefs.GetInt("sleeping");
        if (i == 0)
        {
            stats.sleeping = false;
        }
        else
        {
            stats.sleeping = true;
        }
    }
}






