﻿using UnityEngine;
using System.Collections;

public class tempInputControl : MonoBehaviour {
	tamagochi_statsHandle t;
	// Use this for initialization
	void Start () {
	
		t = GetComponent<tamagochi_statsHandle> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Alpha1))
		{
			t.SetTimePassed(3600);
		}
		if (Input.GetKeyUp (KeyCode.Alpha2))
			t.SetTimePassed (21600);
		if (Input.GetKeyUp (KeyCode.Alpha3))
			t.SetTimePassed (54000);
		if (Input.GetKeyUp (KeyCode.Alpha4))
			t.SetTimePassed (129600);
		if(Input.GetKeyUp(KeyCode.Space))
		   t.flipSleep();
		if (Input.GetKeyUp (KeyCode.A))
			t.feed ();
	}
}
