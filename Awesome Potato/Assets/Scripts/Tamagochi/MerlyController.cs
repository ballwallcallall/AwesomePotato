﻿using UnityEngine;
using System.Collections;

public class MerlyController : MonoBehaviour {
    public float timeLeft = 0.0f;
    public Animator anim;
    public int currentState = 1;
    public float stayTime = 3.0f;
	// Use this for initialization
	void Start () {
        timeLeft = 2.0f;
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        timeLeft -= Time.deltaTime;
        if (timeLeft <= 0)
        {
            switch (currentState)
            {
                case 1:
                    anim.SetFloat("Happiness", 80);
                    currentState++;
                    timeLeft = stayTime;
                    break;
                case 2:
                    anim.SetFloat("Happiness", 20);
                    currentState++;
                    timeLeft = stayTime;
                    break;
                case 3:
                    anim.SetBool("Eating", true);
                    currentState++;
                    timeLeft = stayTime;
                    break;
                case 4:
                    anim.SetBool("Eating", false);
                    anim.SetBool("Sleeping", true);
                    timeLeft = stayTime;
                    currentState++;
                    break;
                case 5:
                    anim.SetBool("Sleeping", false);
                    anim.SetTrigger("Tired");
                    currentState++;
                    break;
                case 6:
                    if(anim.GetCurrentAnimatorStateInfo(0).normalizedTime>=0.9f)
                    {
                        anim.SetTrigger("Washing");
                        currentState++;
                    }
                    break;
                case 7:
                    if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
                    {
                        anim.SetTrigger("Sparkle");
                        currentState++;
                    }
                    break;
                case 8:

                    if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
                    {
                        anim.SetTrigger("Smell");
                        anim.SetFloat("Happiness", 50.0f);
                        timeLeft = stayTime;
                        currentState=1;
                    }
                    break;
            }
        }
	}
}
