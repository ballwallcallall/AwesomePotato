﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class FadeScene : MonoBehaviour {

	public Image theImage;
    public float fadeOutTime;
    public float fadeInTime;
    public string TargetLevel;

    void Start()
    {
        FadeOut(fadeOutTime);
    }
    
	public void FadeOut(float time)
	{
		theImage.CrossFadeAlpha (0, time, true);
        theImage.enabled = false;
	}

	public void FadeIn(float time)
	{
        theImage.enabled = true;
		theImage.CrossFadeAlpha (1, time, true);
        StartCoroutine(WaitForTimee(time));
	}

    IEnumerator WaitForTimee(float time)
    {
        yield return new WaitForSeconds(time);
        Application.LoadLevel(TargetLevel);
    }

	/*IEnumerator FadeOutC(float time)
	{
		float timer = time;
		// rate * maxtime = 1 
		float rateOfTimeDecrease = 1 / time;
		Color theColor = theImage.color;
		while(timer > 0)
		{
			timer -= rateOfTimeDecrease * Time.deltaTime;
			theImage.color = new Color(theColor.r, theColor.g, theColor.b, timer / time);
			yield return null;
		}
	}

	IEnumerator FadeInC(float time)
	{
		float timer = 0;
		// rate * maxtime = 1 
		float RateofTimeIncrease = 1 / time;
		Color theColor = theImage.color;
		while(timer <= time)
		{
			timer += RateofTimeIncrease * Time.deltaTime;
			theImage.color = new Color(theColor.r, theColor.g, theColor.b, timer / time);
			yield return null;
		}
	}*/
}
//x / 1 = timer / time