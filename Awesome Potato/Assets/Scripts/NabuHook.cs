﻿using UnityEngine;
using System.Collections;

public class NabuHook : Singleton<NabuHook> {

	protected NabuHook(){}
	
	public int GetDistanceWalkedPast24Hours()
	{
        return 1;
        using (AndroidJavaClass cls_UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
		{
			using (AndroidJavaObject obj_Activity = cls_UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
			{
				obj_Activity.Call("GetStepsFromNabu");
				return obj_Activity.CallStatic<int>("DistanceWalked");
			}
		}
	}
}
