﻿using UnityEngine;
using System.Collections;

public class ExitLevel : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            GameObject lt = GameObject.Find("LevelTransition");
            lt.GetComponent<FadeScene>().FadeIn(lt.GetComponent<FadeScene>().fadeInTime);
        }
    }
}
