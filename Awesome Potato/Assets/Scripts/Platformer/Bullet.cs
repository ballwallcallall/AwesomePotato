﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Destroy(gameObject, 2);
        //transform.GetComponent<Rigidbody2D>().velocity.x = 250;
	}
	
	// Update is called once per frame
	void Update () {

	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Enemy")
        {
            col.transform.GetComponent<Enemy>().Die();
            Destroy(gameObject);
        }
        else if (col.tag != "Player")
        {
            Destroy(gameObject);
        }
    }
}
