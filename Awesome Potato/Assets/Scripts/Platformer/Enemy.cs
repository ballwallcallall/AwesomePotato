﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
    
    private Animator anim;
    private bool aliveEnemy = true;

    private Transform wallCheck;

    private bool facingLeft = true;

	// Use this for initialization
	void Start () {
        anim = GetComponentInChildren<Animator>();
        wallCheck = transform.Find("wallCheck");
        //GetComponent<Rigidbody2D>().AddForce(new Vector2(-900, 0));
	}
	
	// Update is called once per frame
	void Update () {
        if (aliveEnemy)
        {
            if (facingLeft)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(-3.75f, 0));
            }
            else
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(3.75f, 0));
            }
            if (Physics2D.Linecast(transform.position, wallCheck.position, 1 << LayerMask.NameToLayer("Ground")))
            {
                facingLeft = !facingLeft;

                // Multiply the player's x local scale by -1.
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;

            }
        }
	}

    public void Die()
    {
        anim.SetTrigger("Death");
        aliveEnemy = false;
    }
}
