﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
    [HideInInspector]
    public bool facingRight = true;			// For determining which way the player is currently facing.
    [HideInInspector]
    public bool jump = false;				// Condition for whether the player should jump.

    public float moveForce = 365f;			// Amount of force added to move the player left and right.
    public float maxSpeed = 5f;				// The fastest the player can travel in the x axis.
    public AudioClip[] jumpClips;			// Array of clips for when the player jumps.
    public float jumpForce = 1000f;			// Amount of force added when the player jumps.


    private Transform groundCheck;			// A position marking where to check if the player is grounded.
    public bool grounded = false;			// Whether or not the player is grounded.
    private bool falling = false;
    private Animator anim;					// Reference to the player's animator component.

    public Vector3 vel;
    public Vector3 prevPos;

    float hInput;


    public float bulletSpeed = 20f;
    public Rigidbody2D bullet;

    void Awake()
    {
        // Setting up references.
        groundCheck = transform.Find("groundCheck");
        anim = GetComponentInChildren<Animator>();
        prevPos = transform.position;
    }


    void Update()
    {
        // The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        if (grounded && falling)
            falling = false;
        anim.SetBool("Grounded", grounded);

        // If the jump button is pressed and the player is grounded then the player should jump.
        if (Input.GetButtonDown("Jump"))
            Jump();

        if (Input.GetButtonDown("Fire1"))
            Shoot();
            //jump = true;
    }


    void FixedUpdate()
    {
        /*
        // Cache the horizontal input.
        float h = Input.GetAxis("Horizontal");


        // If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
        if (h * GetComponent<Rigidbody2D>().velocity.x < maxSpeed)
            // ... add a force to the player.
            GetComponent<Rigidbody2D>().AddForce(Vector2.right * h * moveForce);

        // If the player's horizontal velocity is greater than the maxSpeed...
        if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > maxSpeed)
            // ... set the player's velocity to the maxSpeed in the x axis.
            GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.x) * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);

        if (h > 0 && !facingRight)
            Flip();
        else if (h < 0 && facingRight)
            Flip();
        */

        Move(Input.GetAxis("Horizontal"));
        Move(hInput);

        // If the player should jump...
        if (jump)
        {
            // Set the Jump animator trigger parameter.
            //anim.SetTrigger("Jump");

            // Play a random jump audio clip.
            //int i = Random.Range(0, jumpClips.Length);
            //AudioSource.PlayClipAtPoint(jumpClips[i], transform.position);

            // Add a vertical force to the player.
            //GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce));

            // Make sure the player can't jump again until the jump conditions from Update are satisfied.
            jump = false;
        }

        anim.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Horizontal")));
        anim.SetBool("Grounded", grounded);
        vel = transform.position - prevPos;
        prevPos = transform.position;


        if (!grounded)
        {
            if (!falling && vel.y < 0)
            {
                anim.SetTrigger("Fall");
                falling = true;
            }
            else if (!falling && vel.y > 0)
            {
                anim.SetTrigger("Jump");
            }
        }
    }

    public void Move(float h)
    {
        if (h * GetComponent<Rigidbody2D>().velocity.x < maxSpeed)
            GetComponent<Rigidbody2D>().AddForce(Vector2.right * h * moveForce);

        if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > maxSpeed)
            GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.x) * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);

        if (h > 0 && !facingRight)
            Flip();
        else if (h < 0 && facingRight)
            Flip();
    }

    public void StartMoving(float h)
    {
        hInput = h;
    }

    public void Jump()
    {
        if (grounded)
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce));
    }

    public void Shoot()
    {
        if (facingRight)
        {
            Rigidbody2D bulletBody = Instantiate(bullet, new Vector3(transform.position.x + 1.0f, transform.position.y, 0), Quaternion.Euler(new Vector3(0, 0, 0))) as Rigidbody2D;
            bulletBody.AddForce(new Vector2(bulletSpeed, 0));
        }
        else
        {
            Rigidbody2D bulletBody = Instantiate(bullet, new Vector3(transform.position.x - 1.0f, transform.position.y, 0), Quaternion.Euler(new Vector3(0, 0, 180))) as Rigidbody2D;
            bulletBody.AddForce(new Vector2(-bulletSpeed, 0));
        }
    }

    void Flip()
    {
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
