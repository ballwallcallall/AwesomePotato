﻿using UnityEngine;
using System.Collections;

public class MapMaker : MonoBehaviour {

    public int segmentLength = 5;
    public int segmentHeight = 1;
    
    //GameObject[] mapArray;

    public Transform[] startingRooms;
    public Transform[] middleRooms;
    public Transform[] endRooms;

	// Use this for initialization
	void Start () {
       // mapArray = new GameObject[segmentLength];
        int[] middleRoomsUsed;

        for (int i = 0; i < segmentLength; i++)
        {
            if (i == 0)
            {
                Instantiate(startingRooms[Random.Range(0, startingRooms.Length)], new Vector3(10 * i, 0, 0), Quaternion.identity);
            }
            else if (i == segmentLength - 1)
            {
                Instantiate(endRooms[Random.Range(0, endRooms.Length)], new Vector3(10 * i, 0, 0), Quaternion.identity);
            }
            else
            {
                int numToAdd = Random.Range(0, startingRooms.Length);
                /*do {
                    numToAdd = Random.Range(0, startingRooms.Length);
                } while middleRoomsUsed.c
                */
                Instantiate(middleRooms[numToAdd], new Vector3(10 * i, 0, 0), Quaternion.identity);
               // middleRoomsUsed.SetValue(

            }
        }
	}

    // Update is called once per frame
    void Update()
    {
	
	}
}
